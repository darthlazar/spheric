#!/bin/sh
#spheric -ogb -halo -Nhalo 5000000 -Mhalo 1.82 -a 1 -b 3 -c 1 -rs 4.42 -rcutoff 100.0 -plummer -Mstar 0.003 -Nstar 1000000 -rp 2.0 -dx -179.72 -dy 132.70 -dz 16.67 -dvx 23.70 -dvy 34.33 -dvz -10.84 -name Antlia2_cm1_ap2-rper14
#spheric -ogb -halo -Nhalo 5000000 -Mhalo 1.82 -a 1 -b 3 -c 1 -rs 4.42 -rcutoff 100.0 -plummer -Mstar 0.003 -Nstar 1000000 -rp 2.0 -dx 12.75 -dy 225.31 -dz -38.77 -dvx 21.816 -dvy 8.688 -dvz -7.616 -name Antlia2_cm1_ap2-lowVic
spheric -ogb -halo -Nhalo 5000000 -Mhalo 1.82 -a 1 -b 3 -c 1 -rs 4.42 -rcutoff 100.0 -plummer -Mstar 0.003 -Nstar 1000000 -rp 2.0 -dx 12.75 -dy 225.31 -dz -38.77 -dvx 27.27 -dvy 10.86 -dvz -9.52 -name Antlia2_cm1_ap2
#spheric -ogb -halo -Nhalo 5000000 -Mhalo 1.82 -a 1 -b 3 -c 1 -rs 4.42 -rcutoff 100.0 -plummer -Mstar 0.003 -Nstar 1000000 -rp 2.0 -dx 12.75 -dy 225.31 -dz -38.77 -dvx 54.54 -dvy 21.72 -dvz -19.04 -name Antlia2_cm1_ap2-2Vinit
#./spheric -ogb -halo -Nhalo 2000000 -Mhalo 1.82 -a 1 -b 3 -c 1 -rs 4.42 -rcutoff 100.0 -plummer -Mstar 0.003 -Nstar 500000 -rp 2.0 -dx 12.75 -dy 225.31 -dz -38.77 -dvx 27.27 -dvy 10.86 -dvz -9.52 -name Antlia2_cm1_ap2-lowerres
#spheric -ogb -halo -Nhalo 5000000 -Mhalo 1.93 -a 1 -b 3 -c 1 -rs 5.70 -rcutoff 100.0 -plummer -Mstar 0.003 -Nstar 1000000 -rp 2.0 -dx 12.75 -dy 225.31 -dz -38.77 -dvx 27.27 -dvy 10.86 -dvz -9.52 -name Antlia2_cm2_ap2
#spheric -ogb -halo -Nhalo 5000000 -Mhalo 1.74 -a 1 -b 3 -c 1 -rs 3.43 -rcutoff 100.0 -plummer -Mstar 0.003 -Nstar 1000000 -rp 2.0 -dx 12.75 -dy 225.31 -dz -38.77 -dvx 27.27 -dvy 10.86 -dvz -9.52 -name Antlia2_cm0_ap2
#spheric -ogb -halo -Nhalo 5000000 -Mhalo 1.878 -a 1 -b 3 -c 1 -rs 5.02 -rcutoff 100.0 -plummer -Mstar 0.003 -Nstar 1000000 -rp 2.0 -dx 12.75 -dy 225.31 -dz -38.77 -dvx 27.27 -dvy 10.86 -dvz -9.52 -name Antlia2_cm1d5_ap2





